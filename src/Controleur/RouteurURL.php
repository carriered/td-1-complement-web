<?php
namespace TheFeed\Controleur;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpFoundation\RequestStack;
use TheFeed\Lib\Conteneur;

use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGenerator;


class RouteurURL
{
    public static function traiterRequete() {

        $requete = Request::createFromGlobals();
        //récupère les informations de la requête depuis les variables globales

        $routes = new RouteCollection();
        // Syntaxes équivalentes
        // "_controller" => ControleurUtilisateur::class . "::afficherFormulaireConnexion",
        // "_controller" => [ControleurUtilisateur::class, "afficherFormulaireConnexion"],

        // Route afficherListe
        $route_publications = new Route("/publications", [
            "_controller" => "\TheFeed\Controleur\ControleurPublication::afficherListe",
        ]);
        $route_publications->setMethods(["GET"]);
        //chaque nom doit etre unique
        $routes->add("afficherListe", $route_publications);

        // Route web/
        $route_web = new Route("/", [
            "_controller" => "\TheFeed\Controleur\ControleurPublication::afficherListe",
        ]);
        $routes->add("web", $route_web);

        // Route afficherFormulaireConnexion
        $route_connexionGET = new Route("/connexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherFormulaireConnexion",
        ]);
        $route_connexionGET->setMethods(["GET"]);
        $routes->add("afficherFormulaireConnexion", $route_connexionGET);

        // Route connecterUtilisateur
        $route_connexionPOST = new Route("/connexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::connecter",
        ]);
        $route_connexionPOST->setMethods(["POST"]);
        $routes->add("connecter", $route_connexionPOST);

        $route_deconnexionGET  = new Route("/deconnexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::deconnecter",
        ]);
        $route_deconnexionGET->setMethods(["GET"]);
        $routes->add("deconnecter", $route_deconnexionGET);

        // Route afficherFormulaireInscription
        $route_inscriptionGET = new Route("/inscription", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherFormulaireCreation",
        ]);
        $route_inscriptionGET->setMethods(["GET"]);
        $routes->add("afficherFormulaireInscription", $route_inscriptionGET);

        // Route creerUtilisateur
        $route_inscriptionPOST = new Route("/inscription", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::creerDepuisFormulaire",
        ]);
        $route_inscriptionPOST->setMethods(["POST"]);
        $routes->add("creerUtilisateur", $route_inscriptionPOST);

        //Route creerDepuisFormulaire
        $route_creerDepuisFormulairePOST = new Route("/publications", [
            "_controller" => "\TheFeed\Controleur\ControleurPublication::creerDepuisFormulaire",
        ]);
        $route_creerDepuisFormulairePOST->setMethods(["POST"]);
        $routes->add("creerDepuisFormulaire", $route_creerDepuisFormulairePOST);

        //Route afficherPublication
        $route_afficherPublicationGET = new Route("/utilisateurs/{idUtilisateur}/publications", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherPublications",
        ]);
        $route_afficherPublicationGET->setMethods(["GET"]);
        $routes->add("afficherPublications", $route_afficherPublicationGET);


        $contexteRequete = (new RequestContext())->fromRequest($requete);
        //recupère les variables globales de la requête

        // Renvoie l'URL .../ressources/css/styles.css, peu importe l'URL courante

        $associateurUrl = new UrlMatcher($routes, $contexteRequete);
        $donneesRoute = $associateurUrl->match($requete->getPathInfo());

        $requete->attributes->add($donneesRoute);

        $resolveurDeControleur = new ControllerResolver();
        $controleur = $resolveurDeControleur->getController($requete);
        //récupère le contrôleur et la méthode à appeler

        $resolveurDArguments = new ArgumentResolver();
        $arguments = $resolveurDArguments->getArguments($requete, $controleur);

        $generateurUrl = new UrlGenerator($routes, $contexteRequete);
        $assistantUrl = new UrlHelper(new RequestStack(), $contexteRequete);

        Conteneur::ajouterService("generateurUrl", $generateurUrl);
        Conteneur::ajouterService("assistantUrl", $assistantUrl);

        call_user_func_array($controleur, $arguments);

    }
}