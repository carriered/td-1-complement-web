<?php

////////////////////
// Initialisation //
////////////////////
use TheFeed\Controleur\RouteurURL;

require_once __DIR__ . '/../vendor/autoload.php';

RouteurURL::traiterRequete();
